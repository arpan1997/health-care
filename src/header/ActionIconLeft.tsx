import React from 'react';
import { Avatar, Title, Caption, Menu } from 'react-native-paper'

import { View, Image, TouchableOpacity } from 'react-native';
import Images from '../utils/images'



const ActionIconLeft = () => {
  
  return (
    <View style={{ flexDirection: 'row' }} >
        <TouchableOpacity >
            <Image source={Images.menu_icon} style={{height:30, width:30, marginLeft:10}}  />
         </TouchableOpacity>
        
    </View>
  );
};

export default ActionIconLeft;