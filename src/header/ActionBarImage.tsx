import React from 'react';

import { View, Image, TouchableOpacity } from 'react-native';
import Images from '../utils/images'



const ActionBarImage = () => {
  
  return (
    <View style={{ flexDirection: 'row' }}>
        <Image
          source={Images.health_logo}
          style={{
            width: 80,
            height: 40,
            borderRadius: 40 / 2,
            marginLeft: 3,
            marginTop:3
          }}
        />
    </View>
  );
};

export default ActionBarImage;