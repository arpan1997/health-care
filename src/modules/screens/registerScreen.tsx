import React, { useState } from 'react'
import { View, Text, Image, TouchableOpacity, TextInput, Modal, ScrollView, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import Images from '../../utils/images'
import styles from '../styles/registerStyles'

interface Props {
    navigation: any

}

interface state {
    username: string;
    password: string;
    
    check: boolean;
    secure?: boolean;
    name: string;
    address:string;
    email:any;
    mobile:any;
    confirmPassword:any;
    dateOfBirth:any;


}

class registerScreen extends React.Component<Props, state> {
    constructor(props: Props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            
            check: true,
            secure: true,
            name:'',
            address:'',
            email:'',
            mobile:'',
            confirmPassword:'',
            dateOfBirth:''

        }
    }
    createTwoButtonAlert = () =>
    Alert.alert(
      "Alert",
      "Are You sure you want to Submit",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: this.onBTN }
      ]
    );

    onBTN = () => {
        Alert.alert(
            "Thank You",
            "Thank You For showing Your Interest",
            [
              
              { text: "OK", onPress: () => console.log("Ok Pressed") }
            ]
          );
        this.props.navigation.navigate('Login')
      }
    
    render() {
        const {
            password, username, check, secure, name, address, email, mobile, confirmPassword, dateOfBirth
        } = this.state;
        return (
            <ScrollView>
            <View style={styles.container}>


                <View>
                    <Text style={styles.design}>
                        Username:
             </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.user_icon} style={styles.ImageStyle} />

                        <TextInput placeholder='Email' returnKeyType='next' value={username}
                            onChangeText={(username) => this.setState({ username })}
                            keyboardType="email-address"
                            autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                            underlineColorAndroid="transparent" />
                    </View>

                    <Text style={styles.design}>
                        Password:
             </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.password_icon} style={styles.ImageStyle} />

                        <TextInput placeholder='Enter Your Password' secureTextEntry={secure}
                            returnKeyType='next'

                            style={{ flex: 1 }} value={password}
                            onChangeText={(pas) => this.setState({ password: pas })} />
                        <TouchableOpacity onPress={() => this.setState({ secure: !secure })}>
                            <Image style={styles.eye}
                                source={secure ? Images.open_eye : Images.close_eye}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.design}>
                        Retype Password:
             </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.password_icon} style={styles.ImageStyle} />

                        <TextInput placeholder='Retype Your Password' secureTextEntry={secure}
                            returnKeyType='next'

                            style={{ flex: 1 }} value={confirmPassword}
                            onChangeText={(copas) => this.setState({ confirmPassword: copas })} />
                        <TouchableOpacity onPress={() => this.setState({ secure: !secure })}>
                            <Image style={styles.eye}
                                source={secure ? Images.open_eye : Images.close_eye}
                            />
                        </TouchableOpacity>

                        
                    </View>
                    <Text style={styles.design}>
                        Name:
                    </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.user_icon} style={styles.ImageStyle} />

                        <TextInput placeholder='Enter Your Name' returnKeyType='next' value={name}
                            onChangeText={(name) => this.setState({ name })}
                            keyboardType='name-phone-pad'
                            autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                            underlineColorAndroid="transparent" />
                    </View>

                    <Text style={styles.design}>
                        Date of Birth:
                    </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.calandar_logo} style={styles.ImageStyle} />

                        <TextInput placeholder='Enter Your DOB' returnKeyType='next' value = {dateOfBirth}
                            onChangeText={() => {}}
                            keyboardType='name-phone-pad'
                            autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                            underlineColorAndroid="transparent" />
                            <TouchableOpacity onPress = {() => {}}>
                            {/* <DateTimePickerModal
                                    isVisible={this.state.dateVisible}
                                    mode="date"
                                    onConfirm={this.handleConfirm}
                                    onCancel={this.hideDatePicker}
                                /> */}
                            
                                
                        </TouchableOpacity>
                        
                    </View>

                    <Text style={styles.design}>
                        Address:
                    </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.address_logo} style={styles.ImageStyle} />

                        <TextInput placeholder='Address' returnKeyType='next' value={address}
                            onChangeText={(address) => this.setState({ address })}
                            keyboardType='default'
                            multiline={true}
                            autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                            underlineColorAndroid="transparent" />
                    </View>

                    <Text style={styles.design}>
                        Email Id:
                    </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.email_logo} style={styles.ImageStyle} />

                        <TextInput placeholder='Enter Your Email Id' returnKeyType='next' value={email}
                            onChangeText={(email) => this.setState({ email })}
                            keyboardType='email-address'
                            autoCapitalize='none' autoCorrect={false} style={{ flex: 1}}
                            underlineColorAndroid="transparent" />
                    </View>

                    <Text style={styles.design}>
                        Contact:
                    </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.call_logo} style={styles.ImageStyle} />

                        <TextInput placeholder='Contact Number' returnKeyType='next' value={mobile}
                            onChangeText={(mobile) => this.setState({ mobile })}
                            keyboardType='number-pad'
                            maxLength={10}
                            autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                            underlineColorAndroid="transparent" />
                    </View>
                    





                    <TouchableOpacity style={styles.button} 
                    disabled={!this.state.username||!this.state.password||!this.state.name
                    ||!this.state.confirmPassword||!this.state.address||!this.state.email||!this.state.mobile
                    } onPress={this.createTwoButtonAlert} >
                        <LinearGradient colors={['#0F2027', '#203A43']} style={styles.signIn}>
                            <Text style={styles.textSign} >Register</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}
                        onPress={() => this.props.navigation.navigate('Login')}>
                        <LinearGradient colors={['#0F2027', '#203A43']} style={styles.signIn}>
                            <Text style={styles.textSign}>Go to Login Screen</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                </View>

            </View >
            </ScrollView>

        );
    }
}

export const Registration = registerScreen;