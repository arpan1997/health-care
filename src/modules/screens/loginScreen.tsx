import React, { useState } from 'react'
import { View, Text, Image, TouchableOpacity, TextInput, ImageBackground, SafeAreaView } from 'react-native'

import LinearGradient from 'react-native-linear-gradient';
import Images from '../../utils/images'
import styles from '../styles/loginStyles'

interface Props {
    navigation: any
}

interface state {
    username: string;
    password: string;
    check: boolean;
    secure?: boolean;
    error: string


}


class loginScreen extends React.Component<Props, state> {
    constructor(props: Props) {
        super(props);
        this.state = {
            username: '',
            password: '',

            check: true,
            secure: true,
            error: ''

        }
    }

    // handleValidationEmail = (text: string) => {
    //     const reg = /^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/;


    //     if (reg.test(text) === true) {
    //         console.warn("Email is Correct");
    //         this.setState({ username: text })
    //     }
    //     else {
    //         this.setState({ username: text })
    //         console.warn("Email is Not Correct");
    //     }
    // }

    // handleValidationPassword = (pas: string) => {
    //     const strongRegex = new RegExp("^(?=.[a-z])(?=.[A-Z])(?=.[0-9])(?=.[!@#\$%\^&\*])(?=.{8,})");
    //     const mediumRegex = new RegExp("^(((?=.[a-z])(?=.[A-Z]))|((?=.[a-z])(?=.[0-9]))|((?=.[A-Z])(?=.[0-9])))(?=.{6,})");

    //     if (strongRegex.test(this.state.password)) {
    //         this.setState({ password: pas });
    //         this.setState({ error: 'strong' })
    //     } else if (mediumRegex.test(this.state.password)) {
    //         this.setState({ password: pas });
    //         this.setState({ error: 'medium' })
    //     } else {
    //         this.setState({ password: pas });
    //         this.setState({ error: 'weak' })
    //     }
    // }

    onCheck = () => {
        if (this.state.username || this.state.password) {
            this.props.navigation.navigate('Home')
        }
    }


    render() {
        const {
            password, username, check, secure
        } = this.state;
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ImageBackground source={Images.health_logo} style={styles.image}>

                    </ImageBackground>
                </View>
                <View style={{ flex: 3, marginTop:50 }}>
                    <View>
                        <Text style={styles.design}>
                            Username:
                         </Text>
                        <View style={styles.SectionStyle}>
                            <Image source={Images.user_icon} style={styles.ImageStyle} />

                            <TextInput placeholder='Enter Your Username' returnKeyType='next' value={username}
                                onChangeText={(username) => this.setState({ username })}
                                keyboardType="email-address"
                                autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                                underlineColorAndroid="transparent" />
                        </View>

                        <Text style={styles.design}>
                            Password:
                        </Text>
                        <View style={styles.SectionStyle}>
                            <Image source={Images.password_icon} style={styles.ImageStyle} />

                            <TextInput placeholder='Enter Your Password' secureTextEntry={secure}
                                returnKeyType='done'

                                style={{ flex: 1 }} value={password}
                                onChangeText={(pas) => this.setState({ password: pas })} />
                            <TouchableOpacity onPress={() => this.setState({ secure: !secure })}>


                                <Image style={styles.eye}
                                    source={secure ? Images.open_eye : Images.close_eye}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={[styles.rememberContainer, { justifyContent: 'space-between' }]}>

                            <View style={styles.rememberContainer}>

                                <TouchableOpacity onPress={() => this.setState({ check: !check })}>
                                    <Image
                                        style={styles.check}
                                        source={check ? Images.unCheck_icon : Images.check_icon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                                <Text style={styles.rememberText}>Remember Me</Text>
                            </View>
                            <Text onPress={() => console.warn('Under Development')}
                                style={styles.forgotText}>Forgot Password</Text>
                        </View>


                        <TouchableOpacity style={styles.button}
                            disabled={!this.state.username || !this.state.password}
                            onPress={this.onCheck}>
                            <LinearGradient colors={['#0F2027', '#203A43']} style={styles.signIn}>
                                <Text style={styles.textSign}>Sign In</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        <View style={styles.signUp}>
                            <Text>Don't have an Account ?</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Registration')}>
                                <Text> Register Now</Text>
                            </TouchableOpacity>
                        </View>


                    </View>

                </View>
            </SafeAreaView >


        );
    }
}

export const Login = loginScreen;