//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import Images from '../../utils/images'


interface Props {
    navigation: any

}

interface state {
    username: string;
    password: string;

    check: boolean;
    secure?: boolean;
    name: string;
    address: string;
    email: any;
    mobile: any;
    confirmPassword: any;
    dateOfBirth: any;


}

// create a component
class editProfile extends Component<Props, state> {
    constructor(props: Props) {
        super(props);
        this.state = {
            username: '',
            password: '',

            check: true,
            secure: true,
            name: '',
            address: '',
            email: '',
            mobile: '',
            confirmPassword: '',
            dateOfBirth: ''

        }
    }
    render() {
        const {
            password, username, check, secure, name, address, email, mobile, confirmPassword, dateOfBirth
        } = this.state;
        return (
            <View style={styles.container}>
                <Text style={styles.design}>
                    Name:
                </Text>
                <View style={styles.SectionStyle}>
                    <Image source={Images.user_icon} style={styles.ImageStyle} />

                    <TextInput placeholder='Enter Your Name' returnKeyType='next' value={name}
                        onChangeText={(name) => this.setState({ name })}
                        keyboardType='name-phone-pad'
                        autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                        underlineColorAndroid="transparent" />
                </View>
                <Text style={styles.design}>
                    Address:
                    </Text>
                <View style={styles.SectionStyle}>
                    <Image source={Images.address_logo} style={styles.ImageStyle} />

                    <TextInput placeholder='Address' returnKeyType='next' value={address}
                        onChangeText={(address) => this.setState({ address })}
                        keyboardType='default'
                        multiline={true}
                        autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                        underlineColorAndroid="transparent" />
                </View>
                <Text style={styles.design}>
                    Contact:
                    </Text>
                <View style={styles.SectionStyle}>
                    <Image source={Images.call_logo} style={styles.ImageStyle} />

                    <TextInput placeholder='Contact Number' returnKeyType='next' value={mobile}
                        onChangeText={(mobile) => this.setState({ mobile })}
                        keyboardType='number-pad'
                        maxLength={10}
                        autoCapitalize='none' autoCorrect={false} style={{ flex: 1 }}
                        underlineColorAndroid="transparent" />
                </View>
                <Text style={styles.design}>
                        Password:
             </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.password_icon} style={styles.ImageStyle} />

                        <TextInput placeholder='Enter Your Password' secureTextEntry={secure}
                            returnKeyType='next'

                            style={{ flex: 1 }} value={password}
                            onChangeText={(pas) => this.setState({ password: pas })} />
                        <TouchableOpacity onPress={() => this.setState({ secure: !secure })}>
                            <Image style={styles.eye}
                                source={secure ? Images.open_eye : Images.close_eye}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.design}>
                        Retype Password:
             </Text>
                    <View style={styles.SectionStyle}>
                        <Image source={Images.password_icon} style={styles.ImageStyle} />

                        <TextInput placeholder='Enter Your Password' secureTextEntry={secure}
                            returnKeyType='next'

                            style={{ flex: 1 }} value={password}
                            onChangeText={(pas) => this.setState({ password: pas })} />
                        <TouchableOpacity onPress={() => this.setState({ secure: !secure })}>
                            <Image style={styles.eye}
                                source={secure ? Images.open_eye : Images.close_eye}
                            />
                        </TouchableOpacity>
                    </View>

                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity style={styles.button}>
                        <LinearGradient colors={['#0F2027', '#203A43']} style={styles.edit}>
                            <Text style={styles.textSign}>Update Profile</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#000',
        height: 40,
        borderRadius: 5,
        margin: 10,
        elevation: 8

    },

    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    design: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 10,
        marginLeft: 15
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        marginTop: 20,
        padding: 5,
        paddingLeft: 10,
        fontSize: 20,
        color: 'white',
    },
    textSign: {
        color: 'white',
        fontWeight: 'bold'
    },
    edit: {
        width: 150,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        flexDirection: 'row',
        elevation: 8

    },
    eye: {
        padding: 10,
        margin: 5,
        marginLeft: 80,
        height: 25,
        width: 25,
        resizeMode: 'contain',
    },
});

//make this component available to the app
export const Edit = editProfile;
