//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { Avatar, Title, Caption } from 'react-native-paper'
import images from '../../utils/images';
import styles from '../styles/profileStyles'

interface Props {
    navigation:any
}

// create a component
class profileScreen extends Component <Props> {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.userInfoSection}>
                    <View style={{ flexDirection: 'row', marginTop: 15 }}>
                        <View style={{ marginLeft: 20 }}>
                            <Title style={[styles.title, { marginTop: 15, marginBottom: 5 }]}>Arpan Govila</Title>
                            <Caption style={styles.caption}>@arpan97</Caption>
                        </View>
                    </View>
                </View>
                <View style={styles.userInfoSection}>
                    <View style={styles.row}>
                        <Image source={images.location_logo} style={styles.image} />
                        <Text style={{ color: '#777777', marginLeft: 20 }}>Gwalior, India</Text>
                    </View>
                    <View style={styles.row}>
                        <Image source={images.call_logo} style={styles.image} />
                        <Text style={{ color: '#777777', marginLeft: 20 }}>+91-7********9</Text>
                    </View>
                    <View style={styles.row}>
                        <Image source={images.email_logo} style={styles.image} />
                        <Text style={{ color: '#777777', marginLeft: 20 }}>a*************@gmail.com</Text>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity style={styles.button} onPress={()=>this.props.navigation.navigate('Edit Profile')}>
                            <LinearGradient colors={['#0F2027', '#203A43']} style={styles.edit}>
                                <Image source = {images.edit_icon} style={{height:20, width:20, tintColor:'#fff', marginRight:20}} />
                                <Text style={styles.textSign}>Edit Profile</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                </View>
                {/* <View style={styles.infoBoxWrapper}>
                    <View style={[styles.infoBox, { borderRightColor: '#dddddd', borderRightWidth: 1 }]}>
                        <Title>$120</Title>
                        <Image source={images.wallet_logo} style={styles.image} />
                        <Caption>Wallet</Caption>
                    </View>
                    <View style={styles.infoBox}>
                        <Title>0</Title>
                        <Image source={images.shopping_logo} style={styles.image} />
                        <Caption>Order</Caption>
                    </View>
                </View>
                <View style={styles.menuWrapper}>
                    <TouchableOpacity>
                        <View style={styles.menuItem}>
                            <Image source={images.fav_logo} style={styles.image} />
                            <Text style={styles.menuItemText}>Your Favorites</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>

                        <View style={styles.menuItem}>
                            <Image source={images.wallet_logo} style={styles.image} />
                            <Text style={styles.menuItemText}>Payment</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>

                        <View style={styles.menuItem}>
                            <Image source={images.communication_logo} style={styles.image} />
                            <Text style={styles.menuItemText}>Tell Your Friends</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>

                        <View style={styles.menuItem}>
                            <Image source={images.setting_logo} style={styles.image} />
                            <Text style={styles.menuItemText}>Setting</Text>
                        </View>
                    </TouchableOpacity> */}
                {/* </View> */}
            </SafeAreaView>
        );
    }
}

export const Profile = profileScreen;