import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    
    eye: {
        padding: 10,
        margin: 5,
        marginLeft: 80,
        height: 25,
        width: 25,
        resizeMode: 'contain',
    },
    
    rememberContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 10
    },
    check: {
        height: 20,
        width: 20,
        tintColor: '#009387',
        backgroundColor: 'white',
    },
    rememberText: {
        marginLeft: 10,
        color: '#009387'
    },
    forgotText: {
        color: '#009387'
    },
    
    textSign: {
        color: 'white',
        fontWeight: 'bold'
    },
    signIn: {
        width: 150,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        flexDirection: 'row',
        elevation:8

    },
   

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: "white",
    },
    image: {
        flex: 0.8,
        resizeMode: 'cover',
        justifyContent: "center"
    },
    images: {
        flex: 1,
        resizeMode: 'contain',
        width: '100%'
    },
    
    text: {
        color: "green",
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center",
        marginTop: 228,
    },
    text2 : {
        fontSize:15,
        textAlign:'center',
        marginTop:25,
        marginBottom:25
      },
    input: {
        borderWidth: 2,
        borderRadius: 10,
        borderColor: 'blue',
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        padding: 5,
        paddingLeft: 10,
        backgroundColor: `#f8f8ff`,
    },
    button: {
        justifyContent:'center',
        alignItems:'center',
        textAlign: 'center',
        marginTop: 10,
        padding: 5,
        paddingLeft: 10,
        fontSize: 20,
        color: 'white',
    },
    design: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 10,
        marginLeft: 15
    },
   
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#000',
        height: 40,
        borderRadius: 5 ,
        margin: 10,
        elevation:8

    },
     
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode : 'stretch',
        alignItems: 'center'
    },
})
