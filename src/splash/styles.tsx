import { StyleSheet } from 'react-native';
import Utils from '../utils/color';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
 
  logoContainer: {
    marginTop: 260,
    marginLeft: 85,
    
    justifyContent: 'center',
    alignContent: 'center',
  },
  splashLogo: {
    height: '100%',
    width: '100%',
  },
});
