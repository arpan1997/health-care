//import liraries
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
import SplashScreen from 'react-native-splash-screen';
import {styles} from './styles'
import Images from '../utils/images'

interface Props {
  navigation:any
}


class splashScreen extends Component <Props> {
  componentDidMount() {
    SplashScreen.hide();
  }

    handleAnimationEnd = () => {
      this.props.navigation.navigate('Login');
    };
    render() {
        return (
            <View style={styles.container}>
            <Animatable.Image
              delay={500}
              duration={2000}
              animation="zoomIn"
              iterationCount={1}
              source={Images.health_logo}
              resizeMode="contain"
              style={styles.splashLogo}
              useNativeDriver
              onAnimationEnd={this.handleAnimationEnd}
            />
        </View>
        );
    }
}




export default splashScreen;
